public class Task5 {
    public static void main(String[] args) {
        int[] lockArray = new int[10];

        lockArray[0] = getRandomNumber(1,6);
        if(lockArray[0]>=4){lockArray[1] = getRandomNumber(1,4);}
        else{lockArray[1] = getRandomNumber(1, 6);};

        int[] filledArray = fillArray(lockArray);
        resultPrint(filledArray);
    }


    public static int getRandomNumber(int min, int max){
        return (int) ((Math.random() * (max - min)) + min);
    }
    public static int[] fillArray(int[] array) {
        //array[2] = 10 - (array[0] + array[1]);
        for (int i = 0; i < array.length - 2; i++) {
            array[i + 2] = 10 - (array[i] + array[i + 1]);
        }
        return array;
    }



    public static void resultPrint(int[] array) {
        System.out.println("Your code is: ");
        for (int i=0;i<array.length;i++) {
            System.out.println(array[i]);
        }
    }
}

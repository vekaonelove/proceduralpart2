import java.util.Scanner;

public class Task1 { //if GCD(A,B,C) equals 1, they are coprime
    public static void main(String[] args) {
        int a, b, c;

        a = inputIntFromConsole("Enter the value of A: ");
        b = inputIntFromConsole("Enter the value of B: ");
        c = inputIntFromConsole("Enter the value of C: ");

        boolean statusCoprime = areCoprime(a,b,c);

        resultPrint(statusCoprime,a,b,c);
    }
    public static int inputIntFromConsole(String message) {
        int value;
        Scanner sc = new Scanner(System.in);
        System.out.print(message);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.print("Invalid input " + message);
        }
        value = sc.nextInt();
        return value;
    }
    public static int findGCD(int a, int b) {
        int GCD;
        while (a != b) {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        GCD = a;
        return GCD;
    }


    public static int findThreeNumbersGCD(int a, int b, int c){
        int threeNumGCD, primaryGCD;
        primaryGCD = findGCD(a,b);
        threeNumGCD = findGCD(primaryGCD,c);  // recursive method
        return threeNumGCD;
    }
    public static boolean areCoprime(int a, int b, int c){
        if(findThreeNumbersGCD(a,b,c) == 1){
            return true;
        }
        return false;
    }
    public static void resultPrint(boolean status, int a,int b,int c) {
        if(status == true){System.out.printf("%d, %d and %d are coprime.",a,b,c);}
        else{System.out.printf("%d, %d and %d are NOT coprime.",a,b,c);}
    }

}

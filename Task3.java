import java.util.Scanner;

public class Task3 {public static void main(String[] args) {
    int a;

    a = inputIntFromConsole("Enter the value of A: ");
    int[] signsArray = createArray(a);

    boolean statusArmstrong = isArmstrong(a,signsArray);

    resultPrint(statusArmstrong,a);
}
    public static int inputIntFromConsole(String message) {
        int value;
        Scanner sc = new Scanner(System.in);
        System.out.print(message);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.print("Invalid input " + message);
        }
        value = sc.nextInt();
        return value;
    }

    public static int getIntLength(int a){
    int l = 0;
    while (a>=1){
        l+=1;
        a /=10;
    }
    return l;
    }
    public static int[] createArray(int a) {
        int[] arr = new int[getIntLength(a)];
        int i = 0;
        while(a>=1){
            arr[i] = a%10;
            i++;
            a = a/10;
        }
        return arr;
    }


    public static int calculateSum(int[] arr){
        int sum = 0;
        int n = arr.length;
        for(int i:arr){
            sum += Math.pow(i,n);
        }
        return sum;
    }
    public static boolean isArmstrong(int a, int[]arr){
        if(calculateSum(arr) == a){
            return true;
        }
        return false;
    }
    public static void resultPrint(boolean status, int a) {
        if(status == true){System.out.printf("%d is an Armstrong number.",a);}
        else{System.out.printf("%d is NOT an Armstrong number.",a);}
    }

}

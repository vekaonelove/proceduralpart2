import java.security.spec.RSAOtherPrimeInfo;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        int k, m;
        int[] array = new int[]{-2034, 34, 12, 5679, 456, 2, -9, 678, 13, 45680, 234, 222, 7854, 6, -10, -124};
        k = inputIntFromConsole("Enter the value of k: ");
        m = inputIntFromConsole("Enter the value of m: ");

        int[] sumArray = getArrayOfSums(array,k,m);
        sumPrint(array,sumArray);
    }

    public static int inputIntFromConsole(String message) {
        int value;
        Scanner sc = new Scanner(System.in);
        System.out.print(message);
        while (!sc.hasNextInt()) {
            sc.nextLine();
            System.out.print("Invalid input " + message);
        }
        value = sc.nextInt();
        return value;
    }
    public static int getPrimarySum(int[]array,int k){
        int primarySum=0;
        if(k + 2 <= array.length){primarySum = array[k] + array[k+1] + array[k+2];}
        return primarySum;
    }

    public static int[] getArrayOfSums(int[]array,int k,int m) {
        int[] arrayOfSums = new int[(m - k + 1) / 3];
        int c = 0;
        for (int i = k; i < m; i += 3) {
            arrayOfSums[c] = getPrimarySum(array, i);
            c++;
        }
        return arrayOfSums;
    }


    public static void sumPrint(int[] array, int[] sumArray) {
        int sum1 = (array[0]+array[1]+array[2]);
        int sum2 = (array[2]+array[3]+array[4]);
        int sum3 = (array[3]+array[4]+array[5]);
        System.out.println("Sum of 1-3 elements is "+ sum1);
        System.out.println("Sum of 3-5 elements is "+ sum2);
        System.out.println("Sum of 4-6 elements is "+ sum3);

        for (int i: sumArray){
            System.out.println("\nYour sum is " + i);
        }
    }
}
